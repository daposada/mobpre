import axios from 'axios';

export default {

    getdata: async function (url) {
        let data = [];
        const token = localStorage.getItem("sesion");
        const config = {
            headers: {
                Authorization: `Bearer ${token}`
            },
        };
        await axios
            .get(url, config)
            .then((resp) => {
                data = resp.data.results;
            })
            .catch((error) => {
                console.log(error);
            });
        return data;
    },
    //Esta funcion es para el modo oscuro que el estado esta montado en el localstorage
    //solo es llamar la funcion en el mounted() y ya se replicaria el modo oscuro solo
    //si si esta activado, sino, no lo montara porque en el localstorage esta la condicion
    //de que si el modo oscuro esta activado se enviara el estado que es "true" al localstorage 
    //sino se remueve en estado "false" del localstorage
    darkMode() {
        if (localStorage.getItem("boleanDarkVerification")) {
            document.body.classList.toggle("dark");
        }

    },

}
